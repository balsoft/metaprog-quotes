struct _GtkWidgetClass
{
  GInitiallyUnownedClass parent_class;



  guint activate_signal;


  void (*dispatch_child_properties_changed) (GtkWidget *widget,
          guint n_pspecs,
          GParamSpec **pspecs);


  void (* destroy) (GtkWidget *widget);
  void (* show) (GtkWidget *widget);
  void (* show_all) (GtkWidget *widget);
  void (* hide) (GtkWidget *widget);
  void (* map) (GtkWidget *widget);
  void (* unmap) (GtkWidget *widget);
  void (* realize) (GtkWidget *widget);
  void (* unrealize) (GtkWidget *widget);
  void (* size_allocate) (GtkWidget *widget,
    GtkAllocation *allocation);
  void (* state_changed) (GtkWidget *widget,
    GtkStateType previous_state);
  void (* state_flags_changed) (GtkWidget *widget,
    GtkStateFlags previous_state_flags);
  void (* parent_set) (GtkWidget *widget,
    GtkWidget *previous_parent);
  void (* hierarchy_changed) (GtkWidget *widget,
    GtkWidget *previous_toplevel);
  void (* style_set) (GtkWidget *widget,
    GtkStyle *previous_style);
  void (* direction_changed) (GtkWidget *widget,
    GtkTextDirection previous_direction);
  void (* grab_notify) (GtkWidget *widget,
    gboolean was_grabbed);
  void (* child_notify) (GtkWidget *widget,
    GParamSpec *child_property);
  gboolean (* draw) (GtkWidget *widget,
                                cairo_t *cr);


  GtkSizeRequestMode (* get_request_mode) (GtkWidget *widget);

  void (* get_preferred_height) (GtkWidget *widget,
                                                         gint *minimum_height,
                                                         gint *natural_height);
  void (* get_preferred_width_for_height) (GtkWidget *widget,
                                                         gint height,
                                                         gint *minimum_width,
                                                         gint *natural_width);
  void (* get_preferred_width) (GtkWidget *widget,
                                                         gint *minimum_width,
                                                         gint *natural_width);
  void (* get_preferred_height_for_width) (GtkWidget *widget,
                                                         gint width,
                                                         gint *minimum_height,
                                                         gint *natural_height);


  gboolean (* mnemonic_activate) (GtkWidget *widget,
                                         gboolean group_cycling);


  void (* grab_focus) (GtkWidget *widget);
  gboolean (* focus) (GtkWidget *widget,
                                         GtkDirectionType direction);


  void (* move_focus) (GtkWidget *widget,
                                         GtkDirectionType direction);
  gboolean (* keynav_failed) (GtkWidget *widget,
                                         GtkDirectionType direction);


  gboolean (* event) (GtkWidget *widget,
      GdkEvent *event);
  gboolean (* button_press_event) (GtkWidget *widget,
      GdkEventButton *event);
  gboolean (* button_release_event) (GtkWidget *widget,
      GdkEventButton *event);
  gboolean (* scroll_event) (GtkWidget *widget,
      GdkEventScroll *event);
  gboolean (* motion_notify_event) (GtkWidget *widget,
      GdkEventMotion *event);
  gboolean (* delete_event) (GtkWidget *widget,
      GdkEventAny *event);
  gboolean (* destroy_event) (GtkWidget *widget,
      GdkEventAny *event);
  gboolean (* key_press_event) (GtkWidget *widget,
      GdkEventKey *event);
  gboolean (* key_release_event) (GtkWidget *widget,
      GdkEventKey *event);
  gboolean (* enter_notify_event) (GtkWidget *widget,
      GdkEventCrossing *event);
  gboolean (* leave_notify_event) (GtkWidget *widget,
      GdkEventCrossing *event);
  gboolean (* configure_event) (GtkWidget *widget,
      GdkEventConfigure *event);
  gboolean (* focus_in_event) (GtkWidget *widget,
      GdkEventFocus *event);
  gboolean (* focus_out_event) (GtkWidget *widget,
      GdkEventFocus *event);
  gboolean (* map_event) (GtkWidget *widget,
      GdkEventAny *event);
  gboolean (* unmap_event) (GtkWidget *widget,
      GdkEventAny *event);
  gboolean (* property_notify_event) (GtkWidget *widget,
      GdkEventProperty *event);
  gboolean (* selection_clear_event) (GtkWidget *widget,
      GdkEventSelection *event);
  gboolean (* selection_request_event) (GtkWidget *widget,
      GdkEventSelection *event);
  gboolean (* selection_notify_event) (GtkWidget *widget,
      GdkEventSelection *event);
  gboolean (* proximity_in_event) (GtkWidget *widget,
      GdkEventProximity *event);
  gboolean (* proximity_out_event) (GtkWidget *widget,
      GdkEventProximity *event);
  gboolean (* visibility_notify_event) (GtkWidget *widget,
      GdkEventVisibility *event);
  gboolean (* window_state_event) (GtkWidget *widget,
      GdkEventWindowState *event);
  gboolean (* damage_event) (GtkWidget *widget,
                                         GdkEventExpose *event);
  gboolean (* grab_broken_event) (GtkWidget *widget,
                                         GdkEventGrabBroken *event);


  void (* selection_get) (GtkWidget *widget,
        GtkSelectionData *selection_data,
        guint info,
        guint time_);
  void (* selection_received) (GtkWidget *widget,
        GtkSelectionData *selection_data,
        guint time_);


  void (* drag_begin) (GtkWidget *widget,
        GdkDragContext *context);
  void (* drag_end) (GtkWidget *widget,
        GdkDragContext *context);
  void (* drag_data_get) (GtkWidget *widget,
        GdkDragContext *context,
        GtkSelectionData *selection_data,
        guint info,
        guint time_);
  void (* drag_data_delete) (GtkWidget *widget,
        GdkDragContext *context);


  void (* drag_leave) (GtkWidget *widget,
        GdkDragContext *context,
        guint time_);
  gboolean (* drag_motion) (GtkWidget *widget,
        GdkDragContext *context,
        gint x,
        gint y,
        guint time_);
  gboolean (* drag_drop) (GtkWidget *widget,
        GdkDragContext *context,
        gint x,
        gint y,
        guint time_);
  void (* drag_data_received) (GtkWidget *widget,
        GdkDragContext *context,
        gint x,
        gint y,
        GtkSelectionData *selection_data,
        guint info,
        guint time_);
  gboolean (* drag_failed) (GtkWidget *widget,
                                    GdkDragContext *context,
                                    GtkDragResult result);


  gboolean (* popup_menu) (GtkWidget *widget);






  gboolean (* show_help) (GtkWidget *widget,
                                    GtkWidgetHelpType help_type);



  AtkObject * (* get_accessible) (GtkWidget *widget);

  void (* screen_changed) (GtkWidget *widget,
                                       GdkScreen *previous_screen);
  gboolean (* can_activate_accel) (GtkWidget *widget,
                                       guint signal_id);


  void (* composited_changed) (GtkWidget *widget);

  gboolean (* query_tooltip) (GtkWidget *widget,
           gint x,
           gint y,
           gboolean keyboard_tooltip,
           GtkTooltip *tooltip);

  void (* compute_expand) (GtkWidget *widget,
                                       gboolean *hexpand_p,
                                       gboolean *vexpand_p);

  void (* adjust_size_request) (GtkWidget *widget,
                                           GtkOrientation orientation,
                                           gint *minimum_size,
                                           gint *natural_size);
  void (* adjust_size_allocation) (GtkWidget *widget,
                                           GtkOrientation orientation,
                                           gint *minimum_size,
                                           gint *natural_size,
                                           gint *allocated_pos,
                                           gint *allocated_size);

  void (* style_updated) (GtkWidget *widget);

  gboolean (* touch_event) (GtkWidget *widget,
                                           GdkEventTouch *event);

  void (* get_preferred_height_and_baseline_for_width) (GtkWidget *widget,
         gint width,
         gint *minimum_height,
         gint *natural_height,
         gint *minimum_baseline,
         gint *natural_baseline);
  void (* adjust_baseline_request)(GtkWidget *widget,
                                           gint *minimum_baseline,
                                           gint *natural_baseline);
  void (* adjust_baseline_allocation) (GtkWidget *widget,
            gint *baseline);
  void (*queue_draw_region) (GtkWidget *widget,
            const cairo_region_t *region);



  GtkWidgetClassPrivate *priv;


  void (*_gtk_reserved6) (void);
  void (*_gtk_reserved7) (void);
};